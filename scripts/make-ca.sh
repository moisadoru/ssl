#!/usr/bin/env bash

SCRIPT=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPT)
BASEDIR=$(dirname $SCRIPTDIR)
DIR="$BASEDIR/ca"

if [ -f "$DIR/ca.key" ]; then
    echo "CA folder ($DIR) not empty. Manually empty it and try again."
    exit 1
fi
mkdir -p $DIR

# generate private key
openssl genrsa -out $DIR/ca.key 2048 \
    2>/dev/null 

# generate root cert
openssl req -x509 -new -nodes -key $DIR/ca.key -sha256 -days 1825 -out $DIR/ca.pem \
    -subj "/C=RO/ST=B/L=Bucuresti/O=WORK/OU=LOCAL/CN=$USER.ca/emailAddress=admin@localhost" \
    2>/dev/null 

# extract certificate crt form pem
openssl x509 -in $DIR/ca.pem -inform PEM -out $DIR/ca.crt

# add ca to system
if [ "$(uname)" == "Darwin" ]; then
    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $DIR/ca.crt
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    sudo mkdir -p /usr/local/share/ca-certificates/extra
    sudo ln $DIR/ca.crt /usr/local/share/ca-certificates/extra/$USER-ca.crt
    sudo update-ca-certificates
fi

echo 'CA created.'
