#!/bin/bash

function print_usage {
    cat << EOF 
Usage: $0 [-f] [-d] [-i] [-h] [-c] hostname [hostname2 ...]

Arguments:
   -h hostname, mandatory
   -d add local domains in the certficate's alternative names section
   -i add local ip addresses in the certficate's alternative names section
   -f force, will remove any existing certificates before generetig new ones
   -c cleanup, will remove certificate request csr and conf file

If other hostnames are specified, they will also be added to the certificate's alternative names section.

EOF
}

FORCE=""
CLEANUP=""
ADD_IPS=""
ADD_DOMAINS=""
while getopts ":cdfhi" opt; do
    case $opt in
        c) CLEANUP="yes" ;;
        d) ADD_DOMAINS="yes" ;;
        f) FORCE="yes" ;;
        h) print_usage; exit 0 ;;
        i) ADD_IPS="yes" ;;
        \?) echo "Unrecognized option: -$OPTARG" >&2; print_usage; exit 1 ;;
    esac
done
shift $(($OPTIND - 1))

if [ "" == "$1" ]; then
    echo "Missing at least a hostname in the arguments list"
    print_usage
    exit 1
fi

export DOMAIN=$1
shift

export EXTRA=""
DOMAINS="$@"
FQDNS=""

if [ "yes" == "$ADD_DOMAINS" ]; then
    FQDNS=$(hostname --all-fqdns)
    DOMAINS="$DOMAINS $FQDNS"
    DOMAINS=$(echo -e "${DOMAINS// /\\n}" | sort -u)
fi
ITER=2
for NAME in ${DOMAINS[@]}; do  
    EXTRA="${EXTRA}DNS.$ITER = $NAME\n"
    ITER=$(expr $ITER + 1)
done

if [ "yes" == "$ADD_IPS" ]; then
    IPS=$(hostname --all-ip-addresses)
    IPS="127.0.0.1 127.0.1.1 $IPS"
    IPS=$(echo -e "${IPS// /\\n}" | sort -u)
    ITER=1
    for IP in ${IPS[@]}; do  
        EXTRA="${EXTRA}IP.$ITER = $IP\n"
        ITER=$(expr $ITER + 1)
    done
fi

EXTRA=$(echo -e $EXTRA)

SCRIPT=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPT)
BASEDIR=$(dirname $SCRIPTDIR)
CA="$BASEDIR/ca"
DIR="$BASEDIR/certs/$DOMAIN"

if [ -d "$DIR" ]; then
    echo "Domain '$DOMAIN' folder ($DIR) exists."
    if [ "$FORCE" == "" ]; then
        echo "Manually remove it and try again."
        exit 1
    else
        echo "Will remove existing certificate."
        rm -rf $DIR
    fi
fi

mkdir -p $DIR

# create cert file
echo "Generating config file '$DIR/request.conf'."
envsubst < $SCRIPTDIR/request.conf > $DIR/request.conf

# generate server private key
echo "Generating server private key '$DIR/server.key'."
openssl genrsa -out $DIR/server.key 2048 \
    2>/dev/null 

# generate certificate signing request
echo "Generating signing request '$DIR/request.csr'."
openssl req -new -sha256 -key $DIR/server.key -out $DIR/request.csr \
    -config $DIR/request.conf
    2>/dev/null 

# generate server certificate
echo "Generating signed server certificate '$DIR/server.crt'."
openssl x509 -req -in $DIR/request.csr -CA $CA/ca.pem -CAkey $CA/ca.key -CAcreateserial \
    -out $DIR/server.crt -days 1825 -sha256 -extensions req_ext -extfile $DIR/request.conf \
    2>/dev/null 

# cleanup
if [ "$CLEANUP" == "yes" ]; then
    echo "Cleaning up '$DIR/request.*' files."
    rm $DIR/request.*
else
    echo "No cleanup of '$DIR/request.*' files (use -c to enable)."
fi

echo "All done. See folder '$DIR'."
