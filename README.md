# Self-Signed SSL certificates with your own CA.

## Setup
First time, run `scripts/make-ca.sh`, then import the CA's pem file into the trusted CA chain in the the system/browser.

## Create certificates
Just run `scripts/make-cert.sh example.com` (replace example.com with your domain), and use the `certs/example.com/server.*` files in your web-server config.

Enjoy. 